import json
import os

from pathlib import Path
from unittest import TestCase

from metadata_tools.importxls.export_utils import save_exported_datasets_to_file
from metadata_tools.importxls.dish_xls_exporter import DishXlsExporter


class TestDishXlsPartnerExtractor(TestCase):
    def test_dish_export(self):
        exporter = DishXlsExporter()
        #full_file_path = os.path.join(os.path.dirname(__file__), 'resources', 'UL_Extended_Data_Information_Sheet-8.xlsx')

        for dirName, subdirList, fileList in os.walk(os.path.join(os.path.dirname(__file__), 'resources')):
             for fname in fileList:
                if fname.lower().endswith('xls') or fname.lower().endswith('xlsx'):
                    try:
                        full_file_path = os.path.join(dirName, fname)
                        dataset_dict = exporter.export_submission(full_file_path)
                        with open(Path(full_file_path).stem + ".json", 'w') as outfile:
                            save_exported_datasets_to_file(dataset_dict, outfile)
                    except ValueError:
                        print("Could not import {}".format(full_file_path))
        return

# Elixir LU metadata utilities

## Clone the repository

Do not forget to also **clone the submodules!**

```bash
git clone <repository-url>
git submodule update --init
```

## Development environment setup

Create virtual environment:

```bash
mkdir project_venv
python3 -m venv project_venv
source ./project_venv/bin/activate
```

Install dependencies (single brackets are required if you are using `zsh`):

```bash
pip install -e '.[dev]'
```

## Testing

Run tests with:

```bash
python setup.py test
```

## Current Version

**v0.2.0-dev**

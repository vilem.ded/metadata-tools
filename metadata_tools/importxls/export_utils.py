import datetime as dt
import json
import logging
import urllib

from datetime import datetime
from json import dumps
from socket import timeout
from urllib.error import HTTPError, URLError


def process_yes_no_answer(answer):
    """
    convert yes/no answers to boolean we take empty answers as no
    :param xls_data_type_list:
    """
    result = False
    if answer:
        if answer.lower() == 'yes':
            result = True
    return result


def process_yes_no_dontknow_answer(answer):
    """
    convert yes/no/dontknow answers to boolean
    we return empty and dontknow answers as None
    :param xls_data_type_list:
    """
    if answer:
        if answer.lower() == 'yes':
            return True
        elif answer.lower() == 'no':
            return False
        else:
            return None
    else:
        return None


def is_data(sheet):
    str = sheet[1,0] if sheet[1, 0] else ''
    logging.info('Is data sheet ----> {}'.format(str))
    return 'data' in str.lower()

def is_study(sheet):
    str = sheet[1,0] if sheet[1, 0] else ''
    logging.info('Is study sheet ----> {}'.format(str))
    return 'study' in str.lower()

def is_submission(sheet):
    str = sheet[1,0] if sheet[1, 0] else ''
    logging.info('Is submission sheet ----> {}'.format(str))
    return 'submission' in str.lower()


def get_value_list_from_row(sheet, row_idx):
    result = []
    vals = sheet.row[row_idx]
    data_vals = vals[1:]
    for val in data_vals:
        if val:
            result.append(val)
    return result


def process_possible_date(possible_date):
    if isinstance(possible_date, dt.date):
        return possible_date.strftime("%Y-%m-%d")
    elif isinstance(possible_date, int):
        return ""
    else:
        try:
            d = datetime.strptime(possible_date.replace('/','.'), '%d.%m.%Y')
            return d.strftime("%Y-%m-%d")
        except ValueError as e:
            return ""



def get_names_from_string(full_name):
    result = ['', '']
    name = full_name.strip()
    if name_cointains_title(name):
        logging.error(f'Name contains titles: {name}')
    if name.endswith(',') or name.endswith(','):
        name = name[:-1]
    if name is not None:
        if " " in name:
            name_list = name.split(" ")
            result[0] = name_list[0]
            result[1] = " ".join(name_list[1:])
        else:
            result[0] = name
    return result


def name_cointains_title(full_name):
    full_name_dotless = full_name.replace(".", " ").lower()
    titles = ['dr', 'prof', 'mr', 'mrs', 'ms', 'msc', 'mga', 'mph', 'sc', 'drph', 'rndr', 'phd', 'ph', 'msi']
    name_list = full_name_dotless.split(" ")
    res = [ele for ele in titles if(ele in name_list)]
    return bool(res)


def get_lines_from_string(a_string):
    result = []
    stripped = a_string.strip()
    line_list = stripped.splitlines()
    for line in line_list:
        if line:
            result.append(line)
    return result


def get_partners_from_daisy():
    entities_json_str = None
    try:
        urlEntities = urllib.parse.urljoin('https://daisy.lcsb.uni.lu/api/', 'partners')
        with urllib.request.urlopen(urlEntities, timeout=5) as response:
            try:
                data_from_url = response.read().decode('utf-8')
                entities_dict = json.loads(data_from_url)
                if 'results' not in entities_dict.keys():
                    raise ValueError('results key not found')
                entities_json_str = dumps(entities_dict['results'])
            except ValueError as e:
                logging.error('URL not returning valid Json: %s \nError: %s', urlEntities, e)
    except (HTTPError, URLError) as error:
        logging.error('Data not retrieved from URL: %s, \nError: %s', urlEntities, error)
    except timeout:
        logging.error('Socket timed out from URL: %s', urlEntities)
    finally:
        if not entities_json_str:
            logging.info('Defaulting to empty ...')
            entities_json_str = dumps('{}')

    return json.loads(entities_json_str)


def save_exported_datasets_to_file(exported_dataset, output_file):
    if isinstance(exported_dataset, list):
        items = exported_dataset
    else:
        items = [exported_dataset]
    obj = {
        "$schema": "https://raw.githubusercontent.com/elixir-luxembourg/json-schemas/master/schemas/elu-dataset.json",
        "items": items
    }
    return json.dump(obj, output_file, ensure_ascii = False, indent=4)
